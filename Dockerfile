FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /workspace/app
COPY . .
RUN dotnet restore && \
    dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:7.0
WORKDIR /workspace/app
ADD https://github.com/open-telemetry/opentelemetry-dotnet-instrumentation/releases/download/v1.0.0-rc.1/otel-dotnet-auto-install.sh otel-dotnet-auto-install.sh
ENV CORECLR_ENABLE_PROFILING "1"
ENV CORECLR_PROFILER_PATH "/otel-dotnet-auto/linux-x64/OpenTelemetry.AutoInstrumentation.Native.so"
ENV DOTNET_ADDITIONAL_DEPS "/otel-dotnet-auto/AdditionalDeps"
ENV DOTNET_SHARED_STORE "/otel-dotnet-auto/store"
ENV DOTNET_STARTUP_HOOKS "/otel-dotnet-auto/net/OpenTelemetry.AutoInstrumentation.StartupHook.dll"
ENV OTEL_DOTNET_AUTO_HOME "/otel-dotnet-auto"
RUN apt-get update && \
    apt-get install -y curl unzip && \
    sh otel-dotnet-auto-install.sh
COPY --from=build /workspace/app/out .
EXPOSE 5000
ENTRYPOINT ["dotnet", "App.dll"]
