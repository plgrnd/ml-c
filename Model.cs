using Microsoft.ML;
using Microsoft.ML.Data;

namespace ML
{
    public class Model
    {
        private readonly MLContext mlContext;
        private readonly ITransformer model;

        public Model()
        {
           mlContext = new MLContext();
           HouseData[] houseData = {
               new HouseData() { Size = 1.1F, Price = 2.2F },
               new HouseData() { Size = 1.9F, Price = 3.3F },
               new HouseData() { Size = 2.8F, Price = 4.0F },
               new HouseData() { Size = 3.4F, Price = 4.7F }
           };
           IDataView trainingData = mlContext.Data.LoadFromEnumerable(houseData);
           var pipeline = mlContext.Transforms.Concatenate("Features", new[] { "Size" })
               .Append(mlContext.Regression.Trainers.Sdca(labelColumnName: "Price", maximumNumberOfIterations: 100));
           model = pipeline.Fit(trainingData);
        }

        public float Predict(float size)
        {
            var house = new HouseData() { Size = size };
            var prediction = mlContext.Model.CreatePredictionEngine<HouseData, Prediction>(model).Predict(house);
            return prediction.Price;
        }

        public class HouseData
        {
            public float Size { get; set; }
            public float Price { get; set; }
        }

        public class Prediction
        {
            [ColumnName("Score")]
            public float Price { get; set; }
        }
    }
}
